---
title: Backpropagation
subtitle: An introduction
author: Léo Granger
aspectratio: 169
toc: true
linkcolor: true
---

# Context

## Prediction

### Training set $\mathcal{T} = \{(x, y)\}$

+ $x$ input _pattern_
+ $y$ _target_

. . .

### Goal

find a function $f$
$$ f(x) \approx y $$
for each $(x, y) \in \mathcal{T}$

. . .

### How?

+ Class of _parametrized functions_ $f_\theta(x) = f(\theta, x)$
+ _Loss function_ $\ell(y_{\textrm{predicted}}, y_{\textrm{real}})$

::: notes

- Examples of targets
- Non convex
- Expensive to calculate

:::

## Stochastic Gradient Descent

### (Mini Batch) Loss

$$
L_{\textrm{mini-batch}}(\theta, \mathcal{B})
 = \sum_{x \in \mathcal{B}} \ell(f(\theta, x))
$$

### Stochastic Gradient descent

$$\delta \theta_t = \theta_{t+1} - \theta_t
 = - \alpha\frac{\partial L_{\textrm{mini-batch}}}{\partial \theta} (\theta_t, \mathcal{B})$$

::: notes

Pair, triplet, quadruplet etc loss also possible

:::

## Which functions?

- Affine $y = f(\theta, x) = A(\theta) \cdot x + b(\theta)$
- Element-wise (non-linear) without parameters:
 $y = \left[\begin{array}{c} y_1\\ \vdots \\ y_d\end{array}\right]
   = \left[\begin{array}{c} a(x_1)\\ \vdots \\ a(x_d)\end{array}\right]$
- Other
  + Norming: $f(x) = \frac{x}{|x|}$
  + Softmax: $f_{i}(x) = \frac{\exp(x_{i})}{\sum_{i'} \exp(x_{i'})}$
- Composition of the above
$$ f_\theta(x) = f^n ({\theta^n}, f^{n-1} ({\theta^{n-1}}, \cdots f^0 ({\theta^0}, x) \cdots)) $$
with $\theta = (\theta^0, \theta^1, \dots, \theta^n)$

::: notes

- Simplest functions
- Element-wise linear is linear
- Convolution is linear

:::

## Neural nets

- Composition of *linear* and *element-wise non-linear* function
- Simplest element-wise non-linear function? **ReLU**

*Neural Nets are the simplest way to parametrize complicated functions*

- From simplest to more complicated
  + (Fully Connected) Affine + ReLU (Multilayer Perceptron)
  + Conv + ReLU + Affine (VGG)
  + Batch Norm
  + Skip connections (ResNets)

# Backprop

## Setting the stage

### Our Neural net

- Consider a set of _layers_ $f^1(\theta^1, \cdot), f^2(\theta^2, \cdot), \cdots, f^n(\theta^n, \cdot)$

### Intermediate features

 For some $x \in \mathcal{T}$ and some $\theta_t = (\theta^1_t, \theta^2_t, \cdots, \theta^n_t)$

Features
  ~ $y^0 = x$
  ~ $y^k_t = f^k(\theta^k_t, y^{k-1}_t)$

Prediction
  ~ $y^n_t$

::: columns
:::: column

### Loss

$$L(\theta_t, x) = \ell(y^n_t)$$
::::

:::: column

### Gradient descent

$$ \begin{aligned}
\theta_{t+1} - \theta_t &= -\alpha \frac{\partial L}{\partial \theta}(\theta_t, x) \\
\pause
\theta^k_{t+1} - \theta^k_t &= -\alpha \frac{\partial L}{\partial \theta^k}(\theta_t, x)
\end{aligned}$$
::::
:::



::: notes

- Redefine patterns to include targets
- We focus only on one input pattern
- Forward pass
- Layer compatibility: output of previous must match input of next

:::

## Gradient

::: {.columns}
:::: {.column width=0.6}
- How does changing $\theta$ affect $L(\theta, x, y)$?
- What is the contribution of each intermediate feature?

. . .

::::
:::: {.column width=0.38}
- $L(\theta, x) = \ell(y^n)$
- $y^k = f^k(\theta^k, y^{k-1})$
- $\bar{\theta}^k = (\theta^0, \cdots, \theta^k)$
::::
:::

. . .

### Sloppy (physicist) chain rule

. . .

$$ \begin{aligned}
\frac{\partial L}{\partial \theta}
 &= \frac{\partial \ell}{\partial y^n} \cdot \frac{{\rm d} y^n}{{\rm d} \theta} \\
 \pause
 &= \frac{\partial \ell}{\partial y^n} \cdot \left(\frac{\partial y^n}{\partial \theta^n}
   + \frac{\partial y^n}{\partial y^{n-1}} \cdot \frac{{\rm d} y^{n-1}}{{\rm d} \bar{\theta}^{n-1}}\right) \\
 \pause
 &= \frac{\partial \ell}{\partial y^n} \cdot \left(\frac{\partial y^n}{\partial \theta^n}
   + \frac{\partial y^n}{\partial y^{n-1}} \cdot \left(\frac{\partial y^{n-1}}{\partial \theta^{n-1}} + \frac{\partial y^{n-1}}{\partial y^{n-2}} \cdot \left(\frac{\partial y^{n-2}}{\partial \theta^{n-2}} + \cdots\right)\right)\right) \\
\end{aligned} $$

## Don't panic!

$$ \begin{aligned}
\frac{\partial L}{\partial \theta}
 &= \frac{\partial \ell}{\partial y^n} \cdot \left(\frac{\partial y^n}{\partial \theta^n}
   + \frac{\partial y^n}{\partial y^{n-1}} \cdot \left(\frac{\partial y^{n-1}}{\partial \theta^{n-1}} + \frac{\partial y^{n-1}}{\partial y^{n-2}} \cdot \left(\frac{\partial y^{n-2}}{\partial \theta^{n-1}} + \cdots\right)\right)\right) \\
 \pause
 &= \underbrace{\frac{\partial \ell}{\partial y^n} \cdot \frac{\partial y^n}{\partial \theta^n}}_{\frac{\partial L}{\partial \theta^n}} +
 \pause
  \underbrace{\frac{\partial \ell}{\partial y^n} \cdot \frac{\partial y^n}{\partial y^{n-1}} \cdot \frac{\partial y^{n-1}}{\partial \theta^{n-1}}}_{\frac{\partial L}{\partial \theta^{n-1}}} +
 \pause
  \underbrace{
    \frac{\partial \ell}{\partial y^n}
    \cdot \frac{\partial y^n}{\partial y^{n-1}}
    \cdot \frac{\partial y^{n-1}}{\partial y^{n-2}}
    \cdot \frac{\partial y^{n-2}}{\partial \theta^{n-2}}
    }_{\frac{\partial L}{\partial \theta^{n-2}}}
  + \cdots
\end{aligned} $$

## Grab a towel

$$ \begin{aligned}
\frac{\partial L}{\partial \theta}
 &= \underbrace{\underbrace{\frac{\partial \ell}{\partial y^n}}_{\delta^{n}} \cdot \frac{\partial y^n}{\partial \theta^n}}_{\frac{\partial L}{\partial \theta^n}}
  + \underbrace{\underbrace{\frac{\partial \ell}{\partial y^n} \cdot \frac{\partial y^n}{\partial y^{n-1}}}_{\delta^{n-1}} \cdot \frac{\partial y^{n-1}}{\partial \theta^{n-1}}}_{\frac{\partial L}{\partial \theta^{n-1}}}
  + \underbrace{
      \underbrace{
        \frac{\partial \ell}{\partial y^n}
        \cdot \frac{\partial y^n}{\partial y^{n-1}}
        \cdot \frac{\partial y^{n-1}}{\partial y^{n-2}}
        }_{\delta^{n-2}}
        \cdot \frac{\partial y^{n-2}}{\partial \theta^{n-2}}
  }_{\frac{\partial L}{\partial \theta^{n-2}}}
  + \cdots
\end{aligned} $$

. . .

### Backprogagation

::: columns
:::: column
$$ \begin{aligned}
\frac{\partial L}{\partial \theta^k} &= \delta^{k} \cdot \frac{\partial y^k}{\partial \theta^k} \\
\pause
\delta^{n} &= \frac{\partial \ell}{\partial y^n} \\
\delta^{k-1} &= \delta^{k} \cdot \frac{\partial y^k}{\partial y^{k-1}}
\end{aligned} $$
::::

. . .

:::: column
Other point of view
$$ \begin{aligned}
\frac{\partial L}{\partial \theta^k} &= \frac{\partial L}{\partial y^k}\cdot \frac{\partial y^k}{\partial \theta^k} \\
\pause
\delta^k &= \frac{\partial L}{\partial y^{k-1}}
\end{aligned} $$
::::
:::

## The less sloppy way

. . .

$$L(\theta_t, x) = \ell(y^n_t) \pause \qquad \qquad y^k_t = f^k(\theta^k_t, y^{k-1}_t) $$

. . .

$$ \begin{aligned}
\frac{\partial L}{\partial \theta}(\theta_t, x)
 &= \pause \frac{\partial \ell}{\partial y^n}(y^n_t) \cdot \pause \frac{{\rm d} y^n_t}{{\rm d}\theta}(\theta_t, x) \\
 \pause
 &= \frac{\partial \ell}{\partial y^n}(y^n_t) \cdot \pause \left(
     \frac{\partial f^n}{\partial \theta^n}(\theta^n_t, y^{n-1}_t)
     + \pause \frac{\partial f^n}{\partial y^{n-1}}(\theta^n_t, y^{n-1}_t)
     \cdot \pause \frac{{\rm d} y^{n-1}_t}{{\rm d}\bar\theta^{n-1}}(\bar\theta^{n-1}_t, x)
 \right) \\
 \pause
 &= \underbrace{\frac{\partial \ell}{\partial y^n}(y^n_t)}_{\delta^{n}_t}
     \cdot \frac{\partial f^n}{\partial \theta^n}(\theta^n_t, y^{n-1}_t)
     + \pause \underbrace{
     \frac{\partial \ell}{\partial y^n}(y^n_t)
     \cdot \frac{\partial f^n}{\partial y^{n-1}}(\theta^n_t, y^{n-1}_t)
     }_{\delta^{n-1}_t}
     \cdot  \frac{\partial f^{n-1}}{\partial \theta^{n-1}}(\theta^{n-1}_t, y^{n-2}_t)
     + \cdots
\end{aligned} $$

---

### Forward pass

$$ \begin{aligned}
y^0 &= x \\
y^k_t &= f^k(\theta^k_t, y^{k-1}_t) \\
L(\theta_t, x) &= \ell(y^n_t)
\end{aligned} $$

### Backward pass

$$ \begin{aligned}
\theta^k_{t+1} - \theta^k_t &= -\alpha \frac{\partial L}{\partial \theta^k}(\theta_t, x) \\
\frac{\partial L}{\partial \theta^k}(\theta_t, x) &= \delta^{k}_t \cdot \frac{\partial f^k}{\partial \theta^k}(\theta^k_t, y^{k-1}_t) \\
\delta^{n}_t &= \frac{\partial \ell}{\partial y^n}(y^n_t)\\
\delta^{k-1}_t &= \delta^{k}_t \cdot \frac{\partial f^{k}}{\partial y^{k-1}}(\theta^{k}_t, y^{k-1}_t) \\
\end{aligned} $$

# Conclusion

## Conclusion

### Note

- [Matrix calculus in *numerator layout*](https://en.wikipedia.org/wiki/Matrix_calculus)
- Check [automatic differenciation](https://en.wikipedia.org/wiki/Automatic_differentiation) and [*autograd*](https://pytorch.org/tutorials/beginner/blitz/autograd_tutorial.html)

. . .

### Summary

- Forget about neurons!
- It's all a about *representation*

. . .

### Outlook

- Formulas for composition of *linear* and *ReLU*?
- Skip connections? (ResNets, LSTM)

### Pull requests welcome
`git clone https://bitbucket.org/leoai4bd/backprop-intro`
