Introduction to Backpropagation and gradient calculation.

Prequisites:

  - pandoc >= 2.2 (e.g. from `conda`)
  - latex, beamer and some other fancy packages, under debian/ubuntu:
    + texlive
    + texlive-extra-utils
    + texlive-fonts-extra
    + texlive-latex-recommended
    + texlive-pictures

Build from sources:

    git clone https://bitbucket.org/leoai4bd/backprop-intro
    cd backprop-intro
    make
