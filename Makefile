slides.pdf: slides.md
	pandoc slides.md -t beamer -o slides.pdf

clean:
	rm -f slides.pdf
